/**
 * 
 */
package test_JUnit;

import static org.junit.Assert.*;

import java.util.Scanner;
import java.util.Stack;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import calculatrice.MoteurRPN;
import exception.CalculException;
import exception.Max_MinException;
import exception.PileVideException;

/**
 * @author user
 *
 */
public class MoteurRPN_Test {
	
	private static MoteurRPN moteur;
	private static Stack<Double> p;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	   // moteur.setP(p);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		p = new Stack<Double> ();
		p.push(12.0);
		p.push(13.0);
		p.push(14.0);
		p.push(15.0);
		
	    moteur = new MoteurRPN();
		for(int i=0; i<p.size(); i++) {
			moteur.enregistrer(p.get(i));
		}

	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		
		
		moteur.setP(null);
	}
	

	@Test
	public void testEnregistrer() {
		
		System.out.println("Test 1: la méthone enregistrer : ");
		assertEquals("Methode enregistrer()",p.size(),moteur.getP().size());
	}
	//
	@Test(expected=Max_MinException .class)
	public void testEnregistrer_Min_Max() throws PileVideException, Max_MinException {
		System.out.println("Test 2: méthone enregistrer ( onvérifie que la valeur ) enregistrer est dans le bon intervalle");
		moteur.enregistrer(4444444444.0);

	}
	
	@Test
	public void calcul_Addition() throws PileVideException, ArithmeticException, CalculException {

		assertTrue(29.0==moteur.calcul("+"));
		
	}
	
	@Test
	public void calcul_Soustraction() throws PileVideException, ArithmeticException, CalculException {
		assertTrue(1.0==moteur.calcul("-"));
	}
	
	@Test
	public void calcul_Multiplication() throws PileVideException, ArithmeticException, CalculException {
		assertTrue(15*14==moteur.calcul("*")); 		
	}
	
	@Test
	public void calcul_Division() throws PileVideException, ArithmeticException, CalculException {
		Double d = (double)15/14;
		assertTrue( d ==moteur.calcul("/")); 		
	}
	
	@Test(expected=ArithmeticException.class)
	public void calcul_Division_Par_Zero() throws PileVideException, ArithmeticException, CalculException, Max_MinException {	

		moteur.enregistrer(0.0);
		moteur.enregistrer(16.0);
        moteur.calcul("/"); 		
	}

	@Test(expected=PileVideException.class)
	public void calcul_Pile_Vide() throws PileVideException, ArithmeticException, CalculException, Max_MinException {
	    for(int i=0;i<moteur.getP().size()-1;i++) {
	    	moteur.getP().pop();
	    }

		moteur.calcul("-"); 

	}

}
