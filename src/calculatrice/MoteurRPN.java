package calculatrice;
import java.util.Stack;
import exception.*;

public class MoteurRPN {	
	private Stack<Double> p;
	public static final Double MAX_VALUE=500.0;
	public static final Double MIN_VALUE=-500.0;
	public MoteurRPN() {
		
		this.p = new Stack<Double> ();
	}
	
	
	

	public Stack<Double> getP() {
		return p;
	}




	public void setP(Stack<Double> p) {
		this.p = p;
	}




	public void enregistrer(Double v) throws Max_MinException, PileVideException {
		Double abs =0.0;
		if(v<0) {
		    abs = v;
			abs *= -1;
		}else {
			abs = v;
		}	
		
		if (abs < MoteurRPN.MIN_VALUE || abs > MoteurRPN.MAX_VALUE) {
			throw new Max_MinException();
			
		}else {
			p.push(v);
			
			System.out.print("Pile : ");
			this.ensembleOperande();
			System.out.println("");
		}	
		
	}
	
	public double calcul(String symbole) throws PileVideException,ArithmeticException,CalculException {
		double result = 0.0 ;
		double v,y;
		if(p.isEmpty())
			throw new PileVideException();
		v = p.pop();
		
		if(p.isEmpty()) {
			p.push(v);
			throw new PileVideException();
		}	
		y = p.pop();
		

			switch (symbole) {
			case "+":
				
				result = Operation.PLUS.eval(v,y);
				
				p.push(result);
				break;
			
			case "-":
				result = Operation.MOINS.eval(v, y);
				p.push(result);
				break;
			
			case "*":
				result = Operation.MULT.eval(v, y);
				p.push(result);
				break;
				
			case "/":
				if(y==0) {
					throw new ArithmeticException();
				}else {
					result = Operation.DIV.eval(v, y);
					p.push(result);
				}

				break;
			}
		
		
			
		return result;
	}
	
	public void ensembleOperande() throws PileVideException {
		if(!p.isEmpty()) {
		int i = p.size();
		while (i > 0) {
			double val = p.get(i-1);
			System.out.print(val+ " ");
			i--;
		}
		}else {
			throw new PileVideException();
		}
	}
	
	
}
