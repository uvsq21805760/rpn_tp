package calculatrice;

import java.util.Scanner;

import exception.*;

public class SaisieRPN {
	
	private MoteurRPN rpn;
	
	
	
	public SaisieRPN() {

		rpn = new MoteurRPN();
	}
	
	public void saisie2() throws PileVideException ,Max_MinException, OperationException, CalculException {
		boolean saisieCorrecte = false;

		do {
			Scanner scan = new Scanner(System.in);

			String valeur = scan.nextLine();	

			try {
				Double d = Double.parseDouble(valeur);	
				rpn.enregistrer(d);

				saisieCorrecte = true;		
				
			}catch (Max_MinException e) {
				System.out.println(e.getMessage());
				saisieCorrecte = false;
			}
			
			catch(NumberFormatException e) {
				
				 try {
					if(!valeur.equals("+") && !valeur.equals("-") && !valeur.equals("*") && !valeur.equals("/") && !valeur.equals("e") && !valeur.equals("E")) {
						throw new OperationException();
					}else {
						saisieCorrecte = true;
						
						if(valeur.equals("+")) {
							
							operation(valeur);
						}else if(valeur.equals("-")) {
							operation(valeur);
						}else if(valeur.equals("*")) {
							operation(valeur);
						}else if(valeur.equals("/")) {
							operation(valeur);
						}else if(valeur.equals("e") || valeur.equals("E")) {
							System.exit(-1);
						}
				}
			
				 }catch(OperationException ex) {
					    System.out.println(ex.getMessage());
						saisieCorrecte = false;
					 
				 }catch(ArithmeticException ae) {
					    System.out.println("Impossible deffectuer une division par 0");
					    System.exit(-1);
				 }
				 catch (PileVideException p_v_e) {
					    System.out.println(p_v_e.getMessage());
						saisieCorrecte = false;
				 }				 
				 catch (CalculException c_e) {
					    System.out.println(c_e.getMessage());
						saisieCorrecte = false;
				 }

					
			}

			
		}while(saisieCorrecte == false);

		

		

	}
	public void operation(String op) throws PileVideException, ArithmeticException, CalculException {
		System.out.print("Pile : ");
		rpn.ensembleOperande();
		System.out.print(op + "  =");
		rpn.calcul(op);

		rpn.ensembleOperande();
		System.out.println("");
	}
	
	/*
	public void saisie() {
		Scanner scan = new Scanner(System.in);
		do {
			System.out.println("entrer un nombre ou une operation : ");
			String valeur = scan.nextLine();
			if (Double.)
			if (double a = scan.nextDouble())
		}while(true);
	}
	Scanner clavier=new Scanner(System.in);
	public SaisieRPN(int o){
		do {
			System.out.println("entrer un nombre / une operation : ");
			rpn.enregistrer(clavier.nextDouble());
			System.out.println("entrer un nombre / une operation : ");
			rpn.enregistrer(clavier.nextDouble());
		    rpn.calcul(clavier.nextDouble());
		}while(clavier.nextLine()!= "exit"); 
			
		
		try {
			 result = rpn.calcul("+");
		} catch(PileVideException ex) {
			ex.printStackTrace();
		}
		
	}*/

}
