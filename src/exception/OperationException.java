package exception;


public class OperationException extends Exception{
	
	public OperationException() {
        super("Operation Non Définie, Choisir une opération parmis les suivantes:{+,-,*,/,e,E}");
    }
    
}
