package exception;

public class CalculException extends Exception{
	public CalculException () {
		super("Impossible d'appliquer une opération binaire car la pile ne contient qu'un seul élément");

	}
	
	
}
