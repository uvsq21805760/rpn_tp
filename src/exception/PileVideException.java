package exception;

public class PileVideException extends PileException {

    public PileVideException() {
        super("La Pile est vide");
    }
}
