package exception;

import calculatrice.MoteurRPN;

public class Max_MinException extends Exception {
	
	public Max_MinException() {
        super("choisir un nombre compris entre"+ MoteurRPN.MIN_VALUE + " et "+ MoteurRPN.MAX_VALUE);
    }
}
